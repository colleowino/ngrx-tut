import { Tutorial } from './model/tutorial.model';

export interface AppState {
  tutorial: Tutorial[];
}
